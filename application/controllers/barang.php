<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Barang extends CI_controller {

	public function __construct()
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("barang_model");
		$this->load->model("jenis_barang_model");
		
		//load validasi
		$this->load->library('form_validation');
		
		//cek sesi login
		$user_login =$this->session->userdata();
		if (count($user_login)<=1){
			redirect("auth/index", "refresh");
		}
	}
	
	public function index()
	
	{
		$this->listBarang();
	}
	
	public function listBarang()
/*	{
		$data['data_barang'] = $this->barang_model->tampilDataBarang();
		$data['content']		= 'forms/list_barang';
		$this->load->view('home2', $data);
	}*/

	{
	// proses cari data
	if (isset($_POST['tombol_cari'])) {
		$data['kata_pencarian'] = $this->input->post('caridata');
		$this->session->set_userdata('session_pencarian_barang', $data['kata_pencarian']);
	} else {
		$data['kata_pencarian'] = $this->session->userdata('session_pencarian_barang');
	}
	
	
	
	{
		$data['data_barang'] = $this->barang_model->tombolpagination($data['kata_pencarian']);
		//$data['data_karyawan'] = $this->Karyawan_model->tampilDataKaryawan();
		$data['content'] = 'forms/list_barang';
		$this->load->view('home2', $data);
	}
}

	public function input()
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDatajenisBarang();
		$data['content']		= 'forms/InputBarang';
		
		//if (!empty($_REQUEST)) {
			//$barang = $this->barang_model;
			//$barang->save();
			//redirect("barang/index", "refresh"); 
		//}
		
		// validasi terlebih dahulu
		$validation = $this->form_validation;
		$validation->set_rules($this->barang_model->rules());
		
		if ($validation->run()) {
			$this->barang_model->save();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil !</div>');
			redirect("barang/index", "refresh");
		}
		
		//$data['content'] = 'forms/input_barang';
		$this->load->view('home2', $data);
	}
	
	   public function detail_barang($kode_barang)
	   {
			$data['detail_barang']	= $this->barang_model->detail($kode_barang);
			$data['content']		= 'forms/detail_barang';
			$this->load->view('home2', $data);   
	   }

	public function Editbarang($kode_barang)
	{
	
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDatajenisBarang();
		$data['detail_barang'] = $this->barang_model->detail($kode_barang);
		$data['content']		= 'forms/Editbarang';
		
		//if (!empty($_REQUEST)) {
			//$barang = $this->barang_model;
			//$barang->update($kode_barang);
			//redirect("barang/index", "refresh"); 
		//}
		// validasi terlebih dahulu
		$validation = $this->form_validation;
		$validation->set_rules($this->barang_model->rules());
		
		if ($validation->run()) {
			$this->barang_model->update($kode_barang);
			$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil !</div>');
			redirect("barang/index", "refresh");
		}
		
		$this->load->view('home2', $data);
	}

	public function deletebarang($kode_barang)
	{
		$m_supplier = $this->barang_model;
		$m_supplier->delete($kode_barang);
		redirect("barang/index", "refresh");
		
	}






}
