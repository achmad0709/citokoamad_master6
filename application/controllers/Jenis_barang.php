<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Jenis_barang extends CI_controller {

	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("jenis_barang_model");
		
		//cek sesi login
		$user_login =$this->session->userdata();
		if (count($user_login)<=1){
			redirect("auth/index", "refresh");
		}
	}
	
	public function index()
	
	{
		$this->listjenisBarang();
	}
	
	public function listjenisbarang()
	
	{


		if (isset($_POST['tombol_cari'])) {
		$data['kata_pencarian'] = $this->input->post('caridata');
		$this->session->set_userdata('session_pencarian_jenisbarang', $data['kata_pencarian']);
	} else {
		$data['kata_pencarian'] = $this->session->userdata('session_pencarian_jenisbarang');
	}
	
	
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tombolpagination($data['kata_pencarian']);
		//$data['data_karyawan'] = $this->Karyawan_model->tampilDataKaryawan();
		$data['content'] = 'forms/listjenisbarang';
		$this->load->view('home2', $data);
	}
	}


	public function input_brg()
	{
		$data['data_jenis_barang'] = $this->jenis_barang_model->tampilDatajenisBarang();
				$data['content']		= 'forms/inputJenisBarang';
		
		/*if (!empty($_REQUEST)) {
			$m_jenisbarang = $this->jenis_barang_model;
			$m_jenisbarang->save();
			redirect("Jenis_barang/index", "refresh"); 
		}*/
		$validation = $this->form_validation;
		$validation->set_rules($this->jenis_barang_model->rules());
		
		if ($validation->run()) {
			$this->jenis_barang_model->save();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil !</div>');
			redirect("jenis_barang/index", "refresh");
		}
		
		$this->load->view('home2', $data);
	}
	
	   public function detail_jenis_barang($kode_jenis)
	   {
			$data['detail_jenis_barang']	= $this->jenis_barang_model->detail($kode_jenis);
			$data['content']		= 'forms/detail_jenis_barang';
			$this->load->view('home2', $data);   
	   }

		public function Editjenisbarang($kode_jenis)
	{
	
		$data['detail_jenis_barang'] = $this->jenis_barang_model->detail($kode_jenis);
		$data['content']		= 'forms/Editjenisbarang';
		/*if (!empty($_REQUEST)) {
			$m_jenisbarang = $this->jenis_barang_model;
			$m_jenisbarang->update($kode_jenis);
			redirect("Jenis_barang/index", "refresh"); 
		}*/
		$validation = $this->form_validation;
		$validation->set_rules($this->jenis_barang_model->rules());
		
		if ($validation->run()) {
			$this->jenis_barang_model->save();
			$this->session->set_flashdata('info', '<div style="color: green">Update Data Berhasil !</div>');
			redirect("jenis_barang/index", "refresh");
		}
		
		$this->load->view('home2', $data);

	}

	public function deletejenisbarang($kode_jenis)
	{
		$m_jenisbarang = $this->jenis_barang_model;
		$m_jenisbarang->delete($kode_jenis);
		redirect("Jenis_barang/index", "refresh");
		
	}
	
}
