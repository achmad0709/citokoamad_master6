<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class Supplier extends CI_controller {

	
	
	
	public function __construct()
	
	{
		
		parent::__construct();
		//load model terkait
		$this->load->model("supplier_model");
		
		//cek sesi login
		$user_login =$this->session->userdata();
		if (count($user_login)<=1){
			redirect("auth/index", "refresh");
		}
		
	}
	
	public function index()
	
	{
		$this->listsupplier();
	}
	
	public function listsupplier()

	{
	// proses cari data
	if (isset($_POST['tombol_cari'])) {
		$data['kata_pencarian'] = $this->input->post('caridata');
		$this->session->set_userdata('session_pencarian_supplier', $data['kata_pencarian']);
	} else {
		$data['kata_pencarian'] = $this->session->userdata('session_pencarian_supplier');
	}
	
	
	{
		$data['data_supplier'] = $this->supplier_model->tombolpagination($data['kata_pencarian']);
		//$data['data_karyawan'] = $this->Karyawan_model->tampilDataKaryawan();
		$data['content'] = 'forms/list_supplier';
		$this->load->view('home2', $data);
	}
}

	public function Input()
	{
		$data['data_supplier'] = $this->supplier_model->tampilDataSupplier();
		$data['content']		= 'forms/InputSupplier';
		
		/*if (!empty($_REQUEST)) {
			$m_supplier = $this->supplier_model;
			$m_supplier->save();
			redirect("supplier/index", "refresh"); 
		}*/

		$validation = $this->form_validation;
		$validation->set_rules($this->supplier_model->rules());
		
		if ($validation->run()) {
			$this->supplier_model->save();
			$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil !</div>');
			redirect("supplier/index", "refresh");
		}
		
		
		$this->load->view('home2', $data);
	}
	
	   public function detailsupplier($kode_supplier)
	   {
			$data['detail_supplier']= $this->supplier_model->detailsupplier($kode_supplier);
			$data['content']		= 'forms/detail_supplier';
			$this->load->view('home2', $data);   
	   }
	   public function Editsupplier($kode_supplier)
	{
	
		$data['detail_supplier'] = $this->supplier_model->detailsupplier($kode_supplier);
		$data['content']		= 'forms/Editsupplier';
		
		/*if (!empty($_REQUEST)) {
			$m_supplier = $this->supplier_model;
			$m_supplier->update($kode_supplier);
			redirect("supplier/index", "refresh"); 
		}*/

		$validation = $this->form_validation;
		$validation->set_rules($this->supplier_model->rules());
		
		if ($validation->run()) {
			$this->supplier_model->update($kode_supplier);
			$this->session->set_flashdata('info', '<div style="color: green">Simpan Data Berhasil !</div>');
			redirect("supplier/index", "refresh");
		}
		
		$this->load->view('home2', $data);
	}
	
	public function deletesupplier($kode_supplier)
	{
		$m_supplier = $this->supplier_model;
		$m_supplier->delete($kode_supplier);
		redirect("supplier/index", "refresh");
		
	}
	
	

	
	
	
	
}
