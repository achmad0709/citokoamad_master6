<?php defined ('BASEPATH') OR exit ('no direct script access allowed');

class karyawan_model extends CI_model
{
	//panggil nama table
	private $_table = "karyawan";
	
	public function tampilDataKaryawan()
	{
		//seperti : select * from <nama_table> "cara 1"
		return $this->db->get($this->_table)->result();
	}
	public function tampilDataKaryawan2()
	{
		// CARA 2
		$query = $this->db->query("SELECT * FROM karyawan WHERE flag = 1");
		return $query->result();
	}
	public function tampilDataKaryawan3()
	{
		// CARA 3
		$this->db->select('*');
		$this->db->order_by('nik', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}
	
	
	public function save()
	{
		
		
		
		
		
	
		
		
		$tgl 			=$this->input->post('tgl');
		$bln 			=$this->input->post('bln');
		$thn 			=$this->input->post('thn');
		$nik_karyawan 	= $this->input->post('nik');
		$tgl_gabung		= $thn . "-" . $bln . "-" . $tgl; 
		
		
		$data['nik']			= $nik_karyawan;
		$data['nama_lengkap']	= $this->input->post('nama_lengkap');
		$data['tempat_lahir']	= $this->input->post('tempat_lahir');
		$data['tgl']		= $tgl_gabung;
		$data['jenis_kelamin']	= $this->input->post('jenis_kelamin');
		$data['alamat']			= $this->input->post('alamat');
		$data['telp']			= $this->input->post('telp');
		$data['kode_jabatan']	= $this->input->post('jabatan');
		$data['flag']			= 1;
		$data['photo']			=$this->uploadPhoto($nik_karyawan);
		
		$this->db->insert($this->_table, $data);
	}
	
	
	public function detail($nik)
	{
		$this->db->select('*');
		$this->db->where('nik', $nik); 
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();	
	}
	public function update($nik)
	{
		$tgl =$this->input->post('tgl');
		$bln =$this->input->post('bln');
		$thn =$this->input->post('thn');
		$tgl_gabung = $thn . "-" . $bln . "-" . $tgl;
		
		$data['nama_lengkap']	= $this->input->post('nama_lengkap');
		$data['tempat_lahir']	= $this->input->post('tempat_lahir');
		$data['tgl']	= $tgl_gabung;
		$data['jenis_kelamin']	= $this->input->post('jenis_kelamin');
		$data['alamat']	= $this->input->post('alamat');
		$data['telp']	= $this->input->post('telp');
		$data['kode_jabatan']	= $this->input->post('jabatan');
		$data['flag']	= 1;
		
		if(!empty($_FILES["image"]["name"])){
			$this->hapusPhoto($nik);
			$foto_karyawan		= $this->uploadPhoto($nik);
			}else{
				$foto_karyawan	= $this->input->post('foto_old');
				}
				$data['photo']	= $foto_karyawan;
		
		$this->db->where('nik', $nik);
		$this->db->update($this->_table, $data);
	}
	public function delete($nik)
	{
		$this->db->where('nik', $nik);
		$this->db->delete($this->_table);
		
	}
	public function rules()
	{
		return[
			[
				'field' => 'nik',
				'label' => 'nik',
				'rules' => 'required|max_length[10]',
				'errors' => [
					'required' => 'NIK Tidak Boleh Kosong.',
					'max_length' => 'NIK Tidak Boleh Lebih Dari 10 Karakter.',
				],
			],
			[
				'field' => 'nama_lengkap',
				'label' => 'nama_lengkap',
				'rules' => 'required',
				'errors' => [
					'required' => 'nama lengkap  Tidak Boleh Kosong.',
				],
			],
			[
				'field' => 'tempat_lahir',
				'label' => 'tempat_lahir',
				'rules' => 'required',
				'errors' => [
					'required' => 'Tempat Lahir Tidak Boleh Kosong.'
				],
			],
			[
				'field' => 'telp',
				'label' => 'telp',
				'rules' => 'required|max_length[10]',
				'errors' => [
					'required' => 'Telepon Tidak Boleh Kosong.',
					'max_length' => 'TeleponTidak Boleh Lebih Dari 10 Karakter.',
				],
			],
			[
				'field' => 'alamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => [
					'required' => 'Alamat Tidak Boleh Kosong.',
				]
			]

			
		
		];
	}

	private function uploadPhoto($nik)
	{
		$date_upload				=date('ymd');
		$config['upload_path']		='./resources/fotokaryawan/';
		$config['allowed_types']	='gif|jpg|png|jpeg';
		$config['file_name']		=$date_upload . '_' . $nik;
		$config['overwrite']		=true;
		$config['max_size']			=1024; // untuk mengatur ukuran foto yang akan di upload (1mb)
		//$config['max_width']		=1024; ini buat ukuran gambar (lebih ke luas dan lebar)
		//$config['max_height']		=768;
		
		/*echo"<pre>";
		print_r($_FILES["image"]); die();
		echo "</pre>";*/
		
		$this->load->library('upload', $config);
		
		if ($this->upload->do_upload('image')){
			$nama_file = $this->upload->data("file_name");
			}else{
				$nama_file = "default.png";
				}
				return $nama_file;
	}
	private function hapusPhoto($nik)
	{
		//cari nama file
		$data_karyawan = $this->detail($nik);
		foreach($data_karyawan as $data){
			$nama_file = $data->photo;
			}
		if ($nama_file != "default.jpeg"){
			$path = "./resources/fotokaryawan/" . $nama_file;
			//bentuk path nya : ./resources/fotokaryawan/namafile.jpeg
			return @unlink($path);
			}
	}
	
		private function tampilDataKaryawanPagination($perpage, $uri, $data_pencarian)
	{
		$this->db->select('*');
		if (!empty($data_pencarian)) {
			$this->db->like('nama_lengkap', $data_pencarian);
		}
		
		$this->db->order_by('nik','asc');
		
		$get_data = $this->db->get($this->_table, $perpage, $uri);
		if ($get_data->num_rows() > 0) {
			return $get_data->result();
		} else {
			return null;
		}
	}
	
	
	
	public function tombolpagination($data_pencarian)
	{
		$this->db->Like('nama_lengkap', $data_pencarian);
		$this->db->from($this->_table);
		$hasil = $this->db->count_all_results();
		
		$pagination['base_url'] = base_url().'Karyawan/listKaryawan/load/';
		$pagination['total_rows'] = $hasil;
		$pagination['per_page'] = "3";
		$pagination['uri_segment'] = 4;
		$pagination['num_links'] = 2;
		
		//custom paging config
		
		$pagination['full_tag_open'] = '<div class="pagination">';
		$pagination['full_tag_close'] = '</div>';
		
		$pagination['first_link'] = 'First Page';
		$pagination['first_tag_open'] = '<span class="firstlink">';
		$pagination['first_tag_close'] = '</span>';
		
		
		$pagination['last_link'] = 'First Page';
		$pagination['last_tag_open'] = '<span class="firstlink">';
		$pagination['last_tag_close'] = '</span>';
		
		$pagination['next_link'] = 'Next Page';
		$pagination['next_tag_open'] = '<span class="lastlink">';
		$pagination['next_tag_close'] = '</span>';
		
		$pagination['prev_link'] = 'Prev Page';
		$pagination['prev_tag_open'] = '<span class="prevlink">';
		$pagination['prev_tag_close'] = '</span>';
		
		$pagination['cur_tag_open'] = '<span class="curlink">';
		$pagination['cur_tag_close'] = '</span>';
		
		$pagination['num_tag_open'] = '<span class="numlink">';
		$pagination['num_tag_close'] = '</span>';
		
		 
		
		$this->pagination->initialize($pagination);
		
		$hasil_pagination = $this->tampilDataKaryawanPagination($pagination['per_page'],
		$this->uri->segment(4),$data_pencarian);
		
		return $hasil_pagination;
		
	}
	
}


