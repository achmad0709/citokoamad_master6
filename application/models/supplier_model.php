<?php defined('BASEPATH') OR exit('No direct script acces allowed');

class supplier_model extends CI_Model
{
	//panggil nama table
	private $_table = "supplier";
	
	public function tampilDataSupplier()
	
	{
		//seperti : select * from <name_table>
		return $this->db->get($this->_table)->result();
	}
	
	public function tampilDataSupplier2()
	
	{
		$query = $this->db->query("SELECT * FROM supplier WHERE flag = 1");
		return $query->result();
	}
	
	public function tampilDataSupplier3()
	
	{
		$this->db->select('*');
		$this->db->order_by('kode_supplier', 'ASC');
		$result = $this->db->get($this->_table);
		return $result->result();
	}


	public function save()
	{
		
		$data['kode_supplier']	= $this->input->post('kode_supplier');
		$data['nama_supplier']	= $this->input->post('nama_supplier');
		$data['alamat']	= $this->input->post('alamat');
		$data['telp']	= $this->input->post('telp');
		$data['flag']	= 1;
		$this->db->insert($this->_table, $data);
	}
	
	
	public function detailsupplier($kode_supplier)
	{
		$this->db->select('*');
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->where('flag', 1);
		$result = $this->db->get($this->_table);
		return $result->result();	
	}

	public function update($kode_supplier)
	{
		$data['nama_supplier']	= $this->input->post('nama_supplier');
		$data['alamat']	= $this->input->post('alamat');
		$data['telp']	= $this->input->post('telp');
		$data['flag']	= 1;
		
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->update($this->_table, $data);
	}

	public function delete($kode_supplier)
	{
		$this->db->where('kode_supplier', $kode_supplier);
		$this->db->delete($this->_table);
		
	}


	public function rules()
	{
		return[
			[
				'field' => 'kode_supplier',
				'label' => 'Kode supplier',
				'rules' => 'required|max_length[10]',
				'errors' => [
					'required' => 'Kode Supplier Tidak Boleh Kosong.',
					'max_length' => 'Kode Barang Tidak Boleh Lebih Dari 10 Karakter.',
				],
			],
			[
				'field' => 'nama_supplier',
				'label' => 'Nama SUpplier',
				'rules' => 'required',
				'errors' => [
					'required' => 'Nama Supplier Tidak Boleh Kosong.',
				],
			],
			[
				'field' => 'alamat',
				'label' => 'Alamat',
				'rules' => 'required',
				'errors' => [
					'required' => 'Alamat Tidak Boleh Kosong.'
				],
			],
			[
				'field' => 'telp',
				'label' => 'Telepon',
				'rules' => 'required|numeric',
				'errors' => [
					'required' => 'Nomor Telepon Tidak Boleh Kosong.',

					'numeric' => 'Nomor Telepon Harus Angka'
				]
			]
		
		];
	}


		private function tampilDataSupplierPagination($perpage, $uri, $data_pencarian)
	{
		$this->db->select('*');
		if (!empty($data_pencarian)) {
			$this->db->like('nama_supplier', $data_pencarian);
		}
		
		$this->db->order_by('kode_supplier','asc');
		
		$get_data = $this->db->get($this->_table, $perpage, $uri);
		if ($get_data->num_rows() > 0) {
			return $get_data->result();
		} else {
			return null;
		}
	}


	public function tombolpagination($data_pencarian)
	{
		$this->db->Like('nama_supplier', $data_pencarian);
		$this->db->from($this->_table);
		$hasil = $this->db->count_all_results();
		
		$pagination['base_url'] = base_url().'supplier/listsupplier/load/';
		$pagination['total_rows'] = $hasil;
		$pagination['per_page'] = "3";
		$pagination['uri_segment'] = 4;
		$pagination['num_links'] = 2;
		
		//custom paging config
		
		$pagination['full_tag_open'] = '<div class="pagination">';
		$pagination['full_tag_close'] = '</div>';
		
		$pagination['first_link'] = 'First Page';
		$pagination['first_tag_open'] = '<span class="firstlink">';
		$pagination['first_tag_close'] = '</span>';
		
		
		$pagination['last_link'] = 'First Page';
		$pagination['last_tag_open'] = '<span class="firstlink">';
		$pagination['last_tag_close'] = '</span>';
		
		$pagination['next_link'] = 'Next Page';
		$pagination['next_tag_open'] = '<span class="lastlink">';
		$pagination['next_tag_close'] = '</span>';
		
		$pagination['prev_link'] = 'Prev Page';
		$pagination['prev_tag_open'] = '<span class="prevlink">';
		$pagination['prev_tag_close'] = '</span>';
		
		$pagination['cur_tag_open'] = '<span class="curlink">';
		$pagination['cur_tag_close'] = '</span>';
		
		$pagination['num_tag_open'] = '<span class="numlink">';
		$pagination['num_tag_close'] = '</span>';
		
		 
		
		$this->pagination->initialize($pagination);
		
		$hasil_pagination = $this->tampilDataSupplierPagination($pagination['per_page'],
		$this->uri->segment(4),$data_pencarian);
		
		return $hasil_pagination;
		
	}
	





}
